# Projet d'INF668

Ce dossier contient mon travail effectué dans le cadre du projet d'UE INF668. L'UE étudie la modélisation de systèmes physiques par des équations différentielles.
Ce projet en particulier vise à employer l'outil de simulation OpenModelica afin de modéliser un tel système.

## Système et motivation

Je propose ici d'étudier l'écoulement de l'eau dans un torrent. Il n'y a pas de motivation particulière, je trouve cela joli et intéressant. On peut imaginer faire varier des paramètres simples en premier lieu (inclinaison de la pente), pour ensuite s'amuser avec l'ajout d'obstacles (rochers, rapides, cascades...).

## Premières intuitions

On peut s'intéresser à la trajectoire d'un petit élément volumique à partir de la source. Cet élément serait caractérisé par sa position et sa vitesse (6 variables d'état donc). Les obstacles seraient modélisés par des contraintes algébriques.

Un travail de documentation est à effectuer en premier lieu pour déterminer les équations décrivant la dynamique.

## Travail bibliographique

Il faut tout d'abord un modèle sur lequel s'appuyer pour le projet. Un premier modèle simplifié est présenté par André Paquier pour l'application à des rivières: il s'agit des équations de Saint Vincent. Ces équations donnent l'évolution dans le temps du débit Q et du niveau de la surface libre z. Toutefois les hypothèses nécessaires à la validité du modèle excluent d'emblée notre cas d'étude: les accélérations verticales doivent être faibles, la vitesse longitudinale homogène, et la pente longitudinale doit être faible également. A. Paquier donne en particulier un premier critère nous permettant de distinguer une "rivière" d'un "torrent": dans le cas d'une rivière, la pente longitudinale n'excède pas les 2%

Anouar Kaouachi propose une thèse sur la modélisation de l'écoulement d'eau sur un canal en marches d'escaliers. Cette modélisation reste intéressante pour constater de certains phénomènes pouvant survenir dans des cas proches de possibles cas rencontrés dans un torrent (rapides, cascades, bien que la modélisation puisse peut-être être invalidée par la hauteur de la cascade). Toutefois, à première vue, le document ne semble pas fournir d'équations permettant de modéliser précisément la dynamique, mais s'intéresse plutôt à des paramètres généraux.

Carole Delenne a publié des slides sur l'hydraulique à surface libre. On trouve en particulier une introduction avec différents paramètres pouvant être étudiés ainsi que différents régimes dont un régime rapidement varié qui devrait nous intéresser pour les torrents. La slide 60 me semble intéressante en particulier car elle donne une idée proche de ce à quoi je m'attendais dans ma première intuition: prendre un volume dit de contrôle et appliquer une équation de conservation, ici le théorème d'Euler.

Cédric Dodard reprend également plusieurs régimes et prend l'exemple de ressauts causés par certains obstacles.

À première lectures de ces quatre documents, je pense qu'une meilleure manière de modéliser le cas proposé consiste à s'intéresser au flux à travers un élément de volume et d'y appliquer un théorème de conservation. Les régimes (déterminées selon les coordonnées de l'élément de volume considéré) modifieront les paramètres influant sur ce bilan de flux.

## Prochaines étapes

Si le sujet est validé, voici les prochaines étapes sur lesquelles je compte m'engager:
 - Travail bibliographique sur la modélisation des flux d'écoulement: en particulier vérifier que prendre un petit élément volumique en tant que système est cohérent, et trouver des équations de dynamique. Trouver comment mathématiquement la pente influe sur la mouvement.
 - Prise en main d'OpenModelica: installation sur mon ordinateur, suivi de quelques tutoriels
 - Première modélisation: le torrent est défini par une pente régulière. Il est infini selon y et borné à -1 < x < 1.

## Journal de bord

#### Dimanche 5 février

Recherche d'articles: 4 trouvés. Beaucoup abordent la modélisation de certaines propriétés, mais je ne trouve pas forcément sur la dynamique même des flux. Tout de même une idée intéressante de faire un bilan de flux. D'après mes lointains souvenirs d'il y a 3 ans en prépa, c'est une méthode couramment utilisée d'ailleurs (peut-être mon instinct physique se fait décadent eheh)

Prise en main de OpenModelica via OMShell (tuto: https://openmodelica.org/doc/OpenModelicaUsersGuide/latest/introduction.html#system-overview):
 - version du compilateur : 1.19.3 (moins récent que la version utilisée dans le tuto)
 - premier constat : lorsque des commandes inexistantes sont tapées dans le shell, il n'y a pas de message d'erreur, bon à savoir pour débugger
 - le répertoire courant du Shell se situe dans /tmp/OpenModelica/
 - une commande résultant correctement renvoie true
 - les fichiers .mo contiennent des fonctions/modèles
 - le plot pour dcmotor renvoie false, mais affiche tout de même le bon résultat
 - pour écrire un script, il faut impérativement éditer directement un fichier car pas de multiline dans le shell
 - Bouncing ball OK
 - CAREFUL for plot : setPlotSilent(false), plotParametric args with no quotemarks

Je m'arrête à la partie destinée à apprendre les bases de syntaxe pour la programmation, je continuerai plus tard.

#### Lundi 6 février

Poursuite et fin de l'introduction brève à OpenModelica avec les boucles, définitions de variables et de fonctions.

#### Lundi 13 février

Documentation sur les équations d'Euler (5). Cela me permet notamment de vérifier si les hypothèses sont cohérentes. En particulier:
 - approximation des milieux continus: je ne me placerai donc pas au niveau des molécules, mais bien sur un petit volume afin d'établir un bilan.
 - écoulement adiabatiques: nous pouvons considérer que, sur une assez courte période de temps, le torrent n'est pas sujet à des échanges thermiques avec le sol / l'air.

Dans notre cas l'équation de continuité et de bilan de la quantité de mouvement permettront d'avoir V et p, sachant que ρ est une constante du problème.
Le système serait donc décrit par les variables d'état V et p (soit 6 variables puisque tridmensionnelles).

Reste encore à savoir déterminer des conditions initiales. Pour V, on peut prendre V = 0 à la source du torrent. Pour p, pas certain de comprendre.

Un point sur la compréhension physique de ces équations:
 - l'équation de continuité, puisque ρ est constant, se résume à un gradient nul pour V. Ce n'est pas ce que nous souhaitons a priori...
 - l'équation de bilan de quantité de mouvement fait intervenir g, ce qui est rassurant car indique que plus l'on descend plus notre vitesse en particulier devrait augmenter du fait de la force de gravité.

Notons également que nous nous placerons en régime permanent, les dérivées temporelles s'annulent donc.

#### Lundi 6 mars

Après une pause assez conséquente, je commence à me poser la question de la modélisation. Une première étape va être de traduire les deux équations d'Euler en OpenModelica. Un problème toutefois: OpenModelica ne gère pas les dérivées partielles. Or, c'est là que réside tout mon problème puisque je considère un régime stationnaire dans lequel mes variables d'état n'évoluent donc plus avec le temps.

L'extension PDEModelica1 ne permet que des systèmes 1D, ce qui serait utile en première approximation (si l'on ne place pas d'obstacle, de sol à surface variable etc.) mais deviendrait trop vite restrictif. J'ai cherché des extensions qui me permettraient plus d'expressivité, mais d'après ce fil de discussion (https://stackoverflow.com/questions/11841105/how-to-use-partial-derivative-in-modelica), il semblerait que ce soit impossible. Je vais discrétiser mes variables d'état à la main.

#### Mardi 7 mars

J'ai testé la simulation de l'équation de la chaleur sur une barre 1D en suivant le snippet proposé par le fil de discussion précédemment cité. Ça marche mais apparemment OpenModelica ne sait pas plotter les tableaux. Je me débrouillerai avec un script python qui prendra en entrée le csv généré par OpenModelica, d'autant plus que dans mon cas le temps ne sera pas à prendre en compte a priori.

J'ai écrit, sans tester pour le moment, les équations de continuité et de bilan de quantité de mouvement avec discrétisation sur 2 dimensions.

#### Mercredi 8 mars

Après résolution des erreurs de syntaxe, j'aboutis à un système sous-déterminé. En raisonnant sur mes variables d'états, j'ai vx, vz, p et 3 équations, donc je ne comprends pas bien ce qu'il se passe encore.

En réfléchissant un peu sur la prochaine étape qui serait d'ajouter les contraintes algébriques correspondant au fond du torrent, je me rends également compte que la surface devrait aussi être modélisée par des contraintes algébriques, sinon tout l'espace sera considéré dans les équations. En fait, pour que le modèle détermine seul la surface, il faudrait considérer également le régime de transition, mais les volumes considérés seraient alors variables.

#### Jeudi 16 mars

Ajout de la dimension selon y (la troisième parce que les deux autres étaient déjà là et que flemme de tout intervertir pour le moment).

Ça ne compile toujours pas: la dérivée fait que pour n = 10, je n'aurai que 9 dérivées, donc il faut ajouter un condition aux limites pas forcément très clean mais qui fait le taff.

## Bibliographie

1. André Paquier. Modélisation des écoulements en rivière en crue : pour une utilisation adaptée des équations de Saint Venant. Sciences de l'environnement. Habilitation à diriger des recherches, Université Claude Bernard Lyon 1, 2002. ⟨tel-02580978⟩

2. Kaouachi, Anouar. (2021). Modélisation de l’écoulement d’eau sur un canal à géométrie complexe types marches d’escalier à faible pente. 10.13140/RG.2.2.16636.67209. 

3. Carole Delenne. Hydraulique à surface libre en régime permanent. École d'ingénieur. France. 2020. ⟨hal-03619263⟩ 

4. Cedric Dodard. Simulation d'un ruisseau par approches phenomenologiques pour la synthese d'images. Synthèse d'image et réalité virtuelle [cs.GR]. 2001. ⟨inria-00598421⟩ 

5. https://fr.wikipedia.org/wiki/%C3%89quations_d%27Euler
