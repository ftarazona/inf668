\begin{thebibliography}{8}
\providecommand{\natexlab}[1]{#1}
\providecommand{\url}[1]{\texttt{#1}}
\expandafter\ifx\csname urlstyle\endcsname\relax
  \providecommand{\doi}[1]{doi: #1}\else
  \providecommand{\doi}{doi: \begingroup \urlstyle{rm}\Url}\fi

\bibitem[Paquier(2002)]{paquier}
Andr{\'e} Paquier.
\newblock \emph{{Mod{\'e}lisation des {\'e}coulements en rivi{\`e}re en crue :
  pour une utilisation adapt{\'e}e des {\'e}quations de Saint Venant}}.
\newblock Habilitation {\`a} diriger des recherches, {Habilitation {\`a}
  diriger des recherches, Universit{\'e} Claude Bernard Lyon 1}, 2002.
\newblock URL \url{https://hal.inrae.fr/tel-02580978}.
\newblock [Departement\_IRSTEA]GMA [TR1\_IRSTEA]11 - VERSEAU / TRANSFEAU.

\bibitem[Delenne(2020)]{delenne:hal-03619263}
Carole Delenne.
\newblock {Hydraulique {\`a} surface libre en r{\'e}gime permanent}.
\newblock Lecture, June 2020.
\newblock URL \url{https://hal.science/hal-03619263}.

\bibitem[eul({\natexlab{a}})]{euler}
Équations d'euler.
\newblock {\natexlab{a}}.
\newblock URL \url{https://fr.wikipedia.org/wiki/%C3%89quations_d%27Euler}.

\bibitem[eul({\natexlab{b}})]{euler_reformulated}
Formules de mécanique des fluides.
\newblock {\natexlab{b}}.
\newblock URL
  \url{https://fr.wikipedia.org/wiki/Formules_de_m%C3%A9canique_des_fluides}.

\bibitem[Šilar et~al.(2017)Šilar, Ježek, and Kofránek]{pdemodelica1}
Jan Šilar, Filip Ježek, and Jiri Kofránek.
\newblock Pdemodelica1: a modelica language extension for partial differential
  equations implemented in openmodelica.
\newblock \emph{International Journal of Modelling and Simulation},
  38:\penalty0 1--10, 12 2017.
\newblock \doi{10.1080/02286203.2017.1404417}.

\bibitem[Dodard(2001)]{dodard:inria-00598421}
Cedric Dodard.
\newblock {Simulation d'un ruisseau par approches phenomenologiques pour la
  synthese d'images}.
\newblock Master's thesis, {Ecole Nationale Superieure d'Informatique et de
  Mathematiques Appliquees de Grenoble (ENSIMAG)}, June 2001.
\newblock URL \url{https://hal.inria.fr/inria-00598421}.

\bibitem[Kaouachi(2021)]{kaouachi}
Anouar Kaouachi.
\newblock \emph{Modélisation de l’écoulement d’eau sur un canal à
  géométrie complexe types marches d’escalier à faible pente}.
\newblock PhD thesis, 05 2021.

\bibitem[Dupont()]{modelisation}
Thibault Dupont.
\newblock URL
  \url{https://daillya.github.io/veryliris/Slides%20Des%20Gens/DUPONT.pdf}.

\end{thebibliography}
