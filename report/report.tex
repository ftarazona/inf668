\documentclass[acmlarge, nonacm=true]{acmart}

\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage{natbib}
\bibliographystyle{unsrtnat}
\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,
    urlcolor=cyan,
}
\urlstyle{same}
\usepackage{listings}
\usepackage{graphicx}

\title{Modélisation d'un torrent de montagne}
\author{Florian Tarazona}

\begin{document}
\maketitle
Ce document constitue mon rapport de projet dans le cadre du cours INF668 portant sur les méthodes et outils de résolutions d'équations algébro-différentielles. Pour plus d'informations, contacter Khalil Gorbal (khalil.gorbal@inria.fr).

\section{Introduction}
Dans ce projet nous proposons une modélisation d'un torrent de montagne. Pourquoi ? Et pourquoi pas ? Il s'agit plutôt d'un prétexte pour apprendre à utiliser les bases de l'outil \textit{OpenModelica}, prendre en main son interface afin de lancer des simulations et exploiter les résultats pour les visualiser, vérifier leur cohérence vis-à-vis des lois physiques modélisées. Le système physique choisi permettra également d'illustrer les limites de l'outil et une manière de les contourner.

On peut tout de même imaginer une motivation à un tel projet. Bien que l'eau, dans vingt, trente ans, il n'y en aura plus, les torrents de montagne restent potentiellement sujets à des phénomènes de crue pouvant s'avérer dangereux. Leur modélisation pourrait permettre de prévenir ou atténuer les dégâts causés.

\subsection{Description du système}

Un premier travail bibliographique permet d'aiguiller vers la description du système permettant une modélisation réaliste. Le travail d'André Paquier (\cite{paquier}) permet notamment d'établir la nécéssité d'une modélisation assez fine contrairement au cas simplifiable d'une rivière. En effet, dans le cas d'une rivière, les variables permettant de décrire l'écoulement varie assez lentement pour que l'on puisse les réduire à un débit et une hauteur de surface. Toutefois notre cas d'étude ne satisfait aux hypothèses permettant une telle abstraction: les accélérations verticales dans un torrent sont trop grands, la vitesse longitudinale ne peut être considérée homogène, \textit{i.e. on ne peut supposer que sur une section la vitesse ne varie que peu}. Enfin la pente longitudinale excède généralement les 2\%.

Carole Delenne (\cite{delenne:hal-03619263}) publie un cours dans lequel on trouve certains exercices se rapprochant de cas plus généraux et par conséquent plus adaptés à notre cas d'étude. L'approche du modèle est de réaliser un bilan de flux et des forces sur un élément de volume. Ainsi notre modèle sera décrit par la vitesse de l'écoulement (champ de vecteurs tridimensionnels) ainsi que par la pression (champ scalaire). La simulation devra donner en particulier l'évolution du premier champ en fonction du temps.

\subsection{Composition de ce rapport}
La suite du document se présente de la manière suivante:
\begin{enumerate}
    \item La section \textit{Modèle} présente les hypothèses physiques sur lesquelles notre modélisation s'appuie. Nous proposerons une discussion sur la cohérence des ces hypothèses vis-à-vis du but recherché. Elle donne ensuite les théorèmes et équations qui permettent de décrire le système et qui seront à implémenter dans \textit{OpenModelica}.
    \item La section \textit{Simulation} étudie les problèmes rencontrés lors de l'implémentation du modèle. En particulier nous discuterons de la problématique de comment implémenter les équations aux dérivées partielles.
    \item La section \textit{Résultats} présente les résultats obtenus et l'outil mis au point afin de les visualiser.
    \item Enfin, nous étudierons les travaux précédents traitant de problèmes similaires en mécanique des fluides.
\end{enumerate}

\section{Modèle}

La description du modèle s'appuie une loi de conservation appliquée à une surface délimitant un volume (\cite{euler}). Une variable intensive de l'eau constituant le torrent suivra une équation de la forme

\begin{equation}
    \frac{\partial \phi}{\partial t} + \nabla\cdot\left(\phi \mathbf{V}\right) = S
\end{equation}

où \(\mathbf{V}\) est la vitesse d'écoulement et \(S\) un terme de production volumique, typiquement un bilan de forces.

En appliquant cette relation à la masse volumique \(\rho\) et à la quantité de mouvement \(\rho \mathbf{V}\), on obtient:

\begin{align}
    \frac{\partial \rho}{\partial t} + \nabla\cdot\left(\rho \mathbf{V}\right) &= 0 \label{eq:mass_conservation}\\
    \frac{\partial\left(\rho V\right)}{\partial t} + \nabla\cdot\left(\rho \mathbf{V}\mathbf{V}\right) &= -\nabla p + \rho \mathbf{g} \label{eq:momentum_conservation}
\end{align}

Il convient toutefois de vérifier certaines hypothèses afin de pouvoir appliquer ces équations à notre système:
\begin{itemize}
    \item L'\textit{approximation des milieux continus} est vérifiée en considérant que les éléments volumiques sont assez grands devant la taille des molécules constituantes du milieu pour pouvoir considérer que les propriétés étudiées (vitesse, pression notamment) sont continues.
    \item La vitesse d'écoulement assez grande dans un torrent et les échelles de temps assez courtes considérées permettent de supposer les écoulements \textit{adiabatiques} notamment car la conduction thermique n'a pas le temps d'entraîner des modifications significatives des propriétés du système.
    \item L'eau peut être considérée comme un \textit{fluide parfait}, sans viscosité.
\end{itemize}

De plus, nous pouvons supposer l'eau constituant notre système \textit{incompressible}, et donc poser l'hypothèse que sa masse volumique est constante vis-à-vis du temps et de l'espace. Nous pouvons alors réécrire \ref*{eq:mass_conservation} et \ref*{eq:momentum_conservation}:

\begin{align}
    \nabla\mathbf{V} &= 0\\
    \frac{\partial \mathbf{V}}{\partial t} + \left(\mathbf{V}\cdot\nabla\right)\mathbf{V} &= -\frac{1}{\rho}\nabla p + \mathbf{g}
\end{align}

Enfin, nous retombons sur les équations proposées en coordonnées cartésiennes dans \cite{euler_reformulated} avec \(\left(x, y, z\right):=\left(x_1, x_2, x_3\right)\) :

\begin{align}
    \sum_{i=1}^{3}{\frac{\partial v_i}{\partial x_i}} &= 0 \label{eq:mass_conservation_reform}\\
    \frac{\partial v_j}{\partial t} + \sum_{i=1}^{3}{v_i\frac{\partial v_j}{\partial x_i}} &= -\frac{1}{\rho}\frac{\partial p}{\partial x_j} + g_i\text{ , j = 1, 2, 3} \label{eq:momentum_conservation_reform}
\end{align}

La question se pose également de se placer ou non en régime stationnaire. Dans ce cas, en particulier, la surface du torrent serait déjà établie. Toutefois, il faudrait lors de la simulation fournir cette donnée au modèle. Or, nous ne savons pas effectuer ce calcul. En ne nous plaçant pas en régime stationnaire, \textit{OpenModelica} effectuera lui-même ce calcul.

\section{Simulation}

Une première analyse permet de vérifier que, grâce aux équations \ref*{eq:mass_conservation_reform} et \ref*{eq:momentum_conservation_reform}, notre système est bien déterminé: nous avons 4 équations non redondantes pour 4 variables : \(v_x, v_y, v_z, p\).

\subsection{Dérivées partielles}

L'implémentation du modèle dans \textit{OpenModelica} a révélé dans le cadre du projet une limitation majeure: l'outil ne propose pas de support pour les équations aux dérivées partielles. L'extension \textit{PDEModelica1} (\cite{pdemodelica1}) permet des dérivées partielles mais avec la limitées à une seule dimension. Cela n'est pas adapté à notre cas de figure. Un \href{https://stackoverflow.com/questions/11841105/how-to-use-partial-derivative-in-modelica}{fil de discussion sur \textit{Stack Overflow}} semble suggérer qu'aucune solution "simple" n'existe afin d'implémenter des dérivées partielles sur plusieurs dimensions. On peut néanmoins y trouver un exemple permettant d'implémenter soi-même de tels modèles en discrétisant l'espace puis en utilisant les dérivées discrètes.\\

Pour un premier essai, nous pouvons essayer de prendre un espace discrétisé en \(10\times 10\times 10\) points. Nous fixons au point de coordonnées \(\left|1, 1, 1\right|\) une vitesse initiale. À la compilation, nous tombons sur l'erreur suivante:
\begin{lstlisting}[language=bash]
    Error: Too few equations, under-determined system. 
    The model has 2919 equation(s) and 4000 variable(s).
\end{lstlisting}
On peut rapidement comprendre cette erreur en notant que \(2916 = 9^3 \times 4\).

En discrétisant l'espace nous multiplions notre nombre de variables par le nombre de points, d'où les désormais 4000 variables. Il faut alors faire attention au nombre d'équations que nous utilisons. La dérivée discrète utilise la variable en deux points distincts: nous aurons une dérivée de moins que de points par dimension! Afin de résoudre le problème, il faut préciser pour chaque variable les une condition aux limites, par exemple pour les 0 de chaque dimension. 

Un autre problème rencontré lors après avoir discrétisé l'espace a été la différence causée par deux méthodes à première vue similaires pour effectuer la dérivée discrète: considérer \(v[i+1] - v[i]\) ou \(v[i] - v[i-1]\). Dans le premier cas, une fois que les équations ont été construites, le compilateur finissait par tomber sur un sous-système sur-déterminé:
\begin{lstlisting}
    Error: An independent subset of the model has imbalanced 
    number of equations (3988) and variables (3888).
\end{lstlisting}
Utiliser la deuxième solution permet en particulier pour les variables non impliquées dans des dérivées discrètes de ne pas être des variables aux 0s de chaque dimension, qui ont été fixées.

Enfin, afin de ne pas avoir de conditions aux limites arbitraires (je soupçonnais cela d'être la cause de l'erreur de simulation), j'ai essayé mettre des dérivées nulles aux limites, ainsi \textit{OpenModelica} pourrait tout de même faire évoluer les variables aux limites de l'espace. Cependant l'algorithme de Pantalides échoue alors et la réduction d'indice est impossible:
\begin{lstlisting}
    Error: Internal error IndexReduction.pantelidesIndexReduction failed! 
    Found empty set of continuous equations. 
    Use -d=bltdump to get more information.
    Error: Internal error Transformation Module PFPlusExt 
    index Reduction Method Pantelides failed!
\end{lstlisting}
Ces dérivées nulles mènent à des équations redondantes, en particulier avec l'équation de conservation de la masse (\ref*{eq:mass_conservation_reform}).

\subsection{Modélisation des obstacles}

Bien que le projet n'ait pas pu aboutir jusque là, on peut s'intéresser à la modélisation des obstacles. Le premier obstacle à implémenter est le lit du torrent. Celui-ci est défini par une zone autorisée pour le système. En dehors de cette zone, la vitesse et la pression ne sont plus définies. Cela signifie qu'il faut réappliquer des conditions aux limites. Générer cela à la main serait bien trop laborieux, il faudrait automatiser cela à l'aide d'un programme générateur qui ne considère que les zones autorisées. Puisque les zones non-autorisées n'interviendront pas dans les calculs, la meilleur solution semble de les fixer à 0.

Un autre problème réside dans le calcul de la condition limite de pression, qui n'est a priori pas connue mais joue un rôle majeur dans la variation de la vitesse par l'équation \ref*{eq:momentum_conservation_reform}.

Si ces problèmes se résolvent, on peut ensuite adapter la méthode utilisée pour le lit du torrent à d'autres obstacles, comme des rochers, des cascades, etc.

\subsection{Surface du torrent}

En fait, la zone autorisée est aussi bornée en haut de l'espace par la surface du torrent. Toutefois cette surface n'est pas connue et doit être calculée par simulation. On ne peut pas considérer ce qui est au-dessus de la surface comme une simple zone interdite, qui elle serait modélisée par des variables d'état nulles en tout temps. La surface doit être libre d'évoluer, ainsi elle ne devrait être défini qu'initialement, à \(t=0\).

\section{Résultats}

Le script \textit{visu.py} permet de générer un suite d'images au format GIF représentant le champ de vecteurs vitesse dans un cube. Il faut modifier le script afin de lui fournir le nom du fichier CSV contenant les données ainsi que le nombre de sections sur chaque dimension. Les colonnes du fichier de données doivent être nommées sous la forme \textit{v(coord)[x,y,z]}.

\textbf{Note:} la visualisation considérera les éléments infinitésimaux identiques pour les trois dimensions.

\subsection{Erreur de simulation}

Malheureusement, la simulation diverge dès sa deuxième étape. La divergence se produit aux limites non contraintes de l'espace. En supprimant ces points, on obtient le champ suivant:
\begin{figure}[h]
    \centering
    \includegraphics[width=10cm]{simu_error.png}
    \caption{Au fur et à mesure que z croît (selon l'axe de la profondeur ici), la vitesse croît selon x plutôt que z, et finirait par diverger si les points extrêmes de l'espaces étaient affichés. De plus la vitesse change de direction selon les points.}
\end{figure}

La modélisation souffre également des conditions fixées aux limites en tout temps, ce qui n'est pas réaliste.

Une meilleure modélisation peut être élaborée mais comme dit précédemment, elle est beaucoup plus laborieuse et le plus simple serait de donner explicitement toutes les équations à \textit{OpenModelica} en les générant au préalable.

\section{État de l'art}

L'écoulement de systèmes aquatiques (ruisseaux, rivières, torrents, canaux, etc.) est un problème classique en mécanique des fluides et est un bon exemple utilisé en mécanique des fluides (cf. \cite{delenne:hal-03619263}).

Toutefois, la modélisation nécessite généralement de distinguer le type de système considéré. On trouve donc différents travaux traitant de la modélisation et de la simulation de rivières (\cite{paquier}), de ruisseaux avec obstacles (\cite{dodard:inria-00598421}) ou encore de systèmes complexes avec par exemples une suite d'obstacles (\cite{kaouachi} sur un écoulement en marches d'escaliers). Les variables considérées varient selon les hypothèses permises par le système et certains phénomènes physiques sont abstraits par des variables "de plus haut niveau".

Le cas d'un torrent est néanmoins un cas plus général en cela que la plupart des hypothèses simplificatrices ne sont plus valides. On doit alors en revenir à la base de la physique derrière mécanique des fluides avec les équations de conservation de Euler (\cite{euler,euler_reformulated}).

Mes recherches bibliographiques ne m'ont pas permis toutefois de trouver des outils préexistants sur \textit{OpenModelica} afin de modéliser des écoulements fluides. \cite{modelisation} met en avant les complexités inhérentes liées à la modélisation des rivières et suggère qu'une approche procédurale serait plus adaptée que la simulation.

\section{Conclusion}

Ce projet a permis de découvrir quelles complexités se cachent derrière la simulation d'écoulement de fluides. Le travail bibliographique a notamment mis en exergue tout l'intérêt des hypothèses simplificatrices autorisées par certains systèmes. La manipulation des équations physiques régissant la mécanique des fluides dans les hypothèses considérées a su mettre en avant des limitations de l'outil utilisé: peut-être que l'approche utilisée avec \textit{OpenModelica} n'est pas si adaptée à la simulation du système considéré.

Faute de temps je n'ai que commencé à proposer un outil de génération du modèle (\textit{modelgen.py}) afin d'ensuite lui ajouter la gestion des obstacles, bien qu'il reste encore des questions d'ordre physique en suspens pour fixer les conditions aux limites dans de tels cas.\\

Le code source pour la génération du modèle, la visualisation des résultats ainsi qu'un Makefile facilitant la prise en main de ces outils se trouve sur GitLab: \href{https://gitlab.com/ftarazona/inf668.git}{https://gitlab.com/ftarazona/inf668.git}

\bibliography{references}

\end{document}