SYST=Torrent2D
SRC=$(SYST).mo
SCRIPT=$(SYST).mos
RES=$(SYST)_res.csv
RES_VISU=$(SYST).gif
MAP=basic
NX=10
NY=10
NZ=10

all: visu

visu: $(RES_VISU)

simu: $(RES)

modelgen: $(SRC)

$(RES_VISU): $(RES)
	python visu/visu.py $(RES) $(NX) $(NY) $(NZ) $(RES_VISU)

$(RES): $(SRC) $(SCRIPT)
	omc $(SCRIPT)

$(SRC):
	python modelgen/modelgen.py $(MAP) $(NX) $(NY) $(NZ) $(SRC)

clean:
	rm -rf *.o *.h *.c *.json *.xml *.log *.makefile *.mo *.csv $(SYST) *.png *.gif *.libs
