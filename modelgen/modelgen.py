import sys

RHO = 1
GX = 0
GY = 0
GZ = 9.81
nx, ny, nz = int(sys.argv[2]), int(sys.argv[3]), int(sys.argv[4])
dx, dy, dz = 0.1, 0.02, 0.01

def der(term, axis, x, y, z):
    if axis == 't':
        return f"der({term}[{x}, {y}, {z}])"
    elif axis == 'x':
        x1 = x-1
        return f"({term}[{x}, {y}, {z}] - {term}[{x1}, {y}, {z}]) / dx"
    elif axis == 'y':
        y1 = y-1
        return f"({term}[{x}, {y}, {z}] - {term}[{x}, {y1}, {z}]) / dy"
    elif axis == 'z':
        z1 = z-1
        return f"({term}[{x}, {y}, {z}] - {term}[{x}, {y}, {z1}]) / dz"
    else:
        print("WARNING: unknown axis {}".format(axis))

def mass_conservation(x, y, z):
    return "{} + {} + {} = 0;".format(
            der('vx', 'x', x, y, z), 
            der('vy', 'y', x, y, z), 
            der('vz', 'z', x, y, z))

def momentum_conservation(a, x, y, z):
    return "{} + vx[{}, {}, {}] * {} + vy[{}, {}, {}] * {} + vz[{}, {}, {}] * {} = -(1/rho) * {} + g{};".format(
            der(f"v{a}", 't', x, y, z),
            x, y, z, der(f"v{a}", 'x', x, y, z),
            x, y, z, der(f"v{a}", 'y', x, y, z),
            x, y, z, der(f"v{a}", 'z', x, y, z),
            der('p', a, x, y, z),
            a)

def raw_constraint(term, val = 0):
    return f"{term} = {val};"

def constraint(term, x, y, z, val = 0):
    return f"{term}[{x}, {y}, {z}] = {val};"

def gen_model(nx, dx, ny, dy, nz, dz, equations):
    preamble = f"""
    parameter Integer nx    = {nx};
    parameter Real dx       = {dx};
    parameter Integer ny    = {ny};
    parameter Real dy       = {dy};
    parameter Integer nz    = {nz};
    parameter Real dz       = {dz};
    parameter Real rho      = {RHO};
    parameter Real gx       = {GX};
    parameter Real gy       = {GY};
    parameter Real gz       = {GZ};

    Real[nx, ny, nz] vx;
    Real[nx, ny, nz] vy;
    Real[nx, ny, nz] vz;
    Real[nx, ny, nz] p;
    """

    equations = "\n\t".join(equations)

    return f"""model Torrent2D
{preamble}

equation

    {equations}

end Torrent2D;"""

def basic_equations(nx, ny, nz):
    equations = []

    for x in range(2, nx+1):
        for y in range(2, ny+1):
            equations.append(constraint('vx', x, y, 1))
            equations.append(constraint('vy', x, y, 1))
            equations.append(constraint('vz', x, y, 1))
            equations.append(constraint('p', x, y, 1))
    for x in range(2, nx+1):
        for z in range(2, nz+1):
            equations.append(constraint('vx', x, 1, z))
            equations.append(constraint('vy', x, 1, z))
            equations.append(constraint('vz', x, 1, z))
            equations.append(constraint('p', x, 1, z))
    for y in range(2, ny+1):
        for z in range(2, nz+1):
            equations.append(constraint('vx', 1, y, z))
            equations.append(constraint('vy', 1, y, z))
            equations.append(constraint('vz', 1, y, z))
            equations.append(constraint('p', 1, y, z))
    for x in range(2, nx+1):
        equations.append(constraint('vx', x, 1, 1))
        equations.append(constraint('vy', x, 1, 1))
        equations.append(constraint('vz', x, 1, 1))
        equations.append(constraint('p', x, 1, 1))
    for y in range(2, ny+1):
        equations.append(constraint('vx', 1, y, 1))
        equations.append(constraint('vy', 1, y, 1))
        equations.append(constraint('vz', 1, y, 1))
        equations.append(constraint('p', 1, y, 1))
    for z in range(2, nz+1):
        equations.append(constraint('vx', 1, 1, z))
        equations.append(constraint('vy', 1, 1, z))
        equations.append(constraint('vz', 1, 1, z))
        equations.append(constraint('p', 1, 1, z))
    equations.append(constraint('vx', 1, 1, 1, 1))
    equations.append(constraint('vy', 1, 1, 1, 1))
    equations.append(constraint('vz', 1, 1, 1, 1))
    equations.append(constraint('p', 1, 1, 1, 0))

    for x in range(2, nx+1):
        for y in range(2, ny+1):
            for z in range(2, nz+1):
                equations.append(mass_conservation(x, y, z))
                equations.append(momentum_conservation('x', x, y, z))
                equations.append(momentum_conservation('y', x, y, z))
                equations.append(momentum_conservation('z', x, y, z))
    return equations

def basic_equations_2(nx, ny, nz):
    for x in range(2, nx+1):
        for y in range(2, ny+1):
            equations.append(raw_constraint(f"vx[{x}, {y}, 2] - vx[{x}, {y}, 1]"))
            equations.append(raw_constraint(f"vy[{x}, {y}, 2] - vy[{x}, {y}, 1]"))
            equations.append(raw_constraint(f"vz[{x}, {y}, 2] - vz[{x}, {y}, 1]"))
            equations.append(raw_constraint(f"p[{x}, {y}, 2] - p[{x}, {y}, 1]"))
    for x in range(2, nx+1):
        for z in range(2, nz+1):
            equations.append(raw_constraint(f"vx[{x}, 2, {z}] - vx[{x}, 1, {z}]"))
            equations.append(raw_constraint(f"vy[{x}, 2, {z}] - vy[{x}, 1, {z}]"))
            equations.append(raw_constraint(f"vz[{x}, 2, {z}] - vz[{x}, 1, {z}]"))
            equations.append(raw_constraint(f"p[{x}, 2, {z}] - p[{x}, 1, {z}]"))
    for y in range(2, ny+1):
        for z in range(2, nz+1):
            equations.append(raw_constraint(f"vx[2, {y}, {z}] - vx[1, {y}, {z}]"))
            equations.append(raw_constraint(f"vy[2, {y}, {z}] - vy[1, {y}, {z}]"))
            equations.append(raw_constraint(f"vz[2, {y}, {z}] - vz[1, {y}, {z}]"))
            equations.append(raw_constraint(f"p[2, {y}, {z}] - p[1, {y}, {z}]"))
    for x in range(2, nx+1):
        equations.append(raw_constraint(f"vx[{x}, 1, 2] - vx[{x}, 1, 1]"))
        equations.append(raw_constraint(f"vy[{x}, 1, 2] - vy[{x}, 1, 1]"))
        equations.append(raw_constraint(f"vz[{x}, 1, 2] - vz[{x}, 1, 1]"))
        equations.append(raw_constraint(f"p[{x}, 1, 2] - p[{x}, 1, 1]"))
    for y in range(2, ny+1):
        equations.append(raw_constraint(f"vx[1, {y}, 2] - vx[1, {y}, 1]"))
        equations.append(raw_constraint(f"vy[1, {y}, 2] - vy[1, {y}, 1]"))
        equations.append(raw_constraint(f"vz[1, {y}, 2] - vz[1, {y}, 1]"))
        equations.append(raw_constraint(f"p[1, {y}, 2] - p[1, {y}, 1]"))
    for z in range(2, nz+1):
        equations.append(raw_constraint(f"vx[2, 1, {z}] - vx[1, 1, {z}]"))
        equations.append(raw_constraint(f"vy[2, 1, {z}] - vy[1, 1, {z}]"))
        equations.append(raw_constraint(f"vz[2, 1, {z}] - vz[1, 1, {z}]"))
        equations.append(raw_constraint(f"p[2, 1, {z}] - p[1, 1, {z}]"))
    equations.append(constraint('vx', 1, 1, 1, 1))
    equations.append(constraint('vy', 1, 1, 1, 1))
    equations.append(constraint('vz', 1, 1, 1, 1))
    equations.append(constraint('p', 1, 1, 1))

    for x in range(2, nx+1):
        for y in range(2, ny+1):
            for z in range(2, nz+1):
                equations.append(mass_conservation(x, y, z))
                equations.append(momentum_conservation('x', x, y, z))
                equations.append(momentum_conservation('y', x, y, z))
                equations.append(momentum_conservation('z', x, y, z))
    return equations

def equations_from_map(constraint_map):
    if constraint_map == "basic":
        return basic_equations(nx, ny, nz)

constraint_map = sys.argv[1]
equations = equations_from_map(constraint_map)

with open(sys.argv[5], 'w') as f:
    f.write(gen_model(nx, dx, ny, dy, nz, dz, equations))
