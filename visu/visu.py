import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import glob
from PIL import Image
import sys

#Nx, Ny, Nz = 10, 10, 10
Nx, Ny, Nz = int(sys.argv[2]), int(sys.argv[3]), int(sys.argv[4])

def read_data(datafile):
    raw_data = pd.read_csv(datafile)
    n_steps = raw_data.shape[0]
    speed = [{} for i in range(n_steps)]
    for x in range(Nx):
        for y in range(Ny):
            for z in range(Nz):
                for a in ['x', 'y', 'z']:
                    colname = "v{}[{},{},{}]".format(a, x+1, y+1, z+1)
                    col = raw_data[colname]
                    for (t, elem) in enumerate(col):
                        speed[t][(x, y, z, a)] = elem
    return speed

def draw_vector_field(vecfield):
    x_a, y_a, z_a = np.meshgrid(np.arange(0, Nx, 1),
                          np.arange(0, Ny, 1),
                          np.arange(0, Nz, 1))
    s_x, s_y, s_z = np.zeros((Nx, Ny, Nz)), np.zeros((Nx, Ny, Nz)), np.zeros((Nx, Ny, Nz))
    for x in range(Nx-1):
        for y in range(Ny-1):
            for z in range(Nz-1):
                s_x[x, y, z] = vecfield[(x, y, z, 'x')]
                s_y[x, y, z] = vecfield[(x, y, z, 'y')]
                s_z[x, y, z] = vecfield[(x, y, z, 'z')]
    ax.quiver(x_a, y_a, z_a, s_x, s_y, s_z, length=0.03, normalize=False)

def make_gif(n_steps):
    frames = [Image.open(f"tmp{i}.png") for i in range(n_steps)]
    frame_one = frames[0]
    frame_one.save(sys.argv[5], format="GIF", append_images=frames, save_all=True, duration=300, loop=0)

speed = read_data(sys.argv[1])
for (step, vecfield) in enumerate(speed):
    ax = plt.figure().add_subplot(projection='3d')
    draw_vector_field(vecfield)
    plt.savefig(f"tmp{step}.png")
    plt.clf()
make_gif(len(speed))
